package com.ferratello.logger

class JenkinsEchoLogger implements FancyLogger, Serializable
{
  private final String BOLD = "1;"
  private final String NORMAL = "0;"
  private final String RED = "31m"
  private final String GREEN = "32m"
  private final String YELLOW = "33m"
  private final String BLUE = "34m"
  private final String BLACK = "30m"

  def jenkins

  JenkinsEchoLogger(jenkins)
  {
    this.jenkins = jenkins
  }

  @Override
  void debug(String message)
  {
    coloredEcho { format(NORMAL, BLACK, message) }
  }

  @Override
  void info(String message)
  {
    coloredEcho { format(BOLD, BLACK, message) }
  }

  @Override
  void suggestion(String message)
  {
    coloredEcho { format(BOLD, BLUE, "💡 " + message)}
  }

  @Override
  void warning(String message)
  {
    coloredEcho { format(BOLD, YELLOW, message) }
  }

  @Override
  void error(String message)
  {
    coloredEcho { format(BOLD, RED, "❌ " + message) }
  }

  @Override
  void success(String message)
  {
    coloredEcho { format(BOLD, GREEN, "✅ " + message) }
  }

  private void coloredEcho(format)
  {
    jenkins.wrap([$class: 'AnsiColorBuildWrapper']) {
      jenkins.echo(format())
    }
  }

  private static String format(String weight, String color, String message)
  {
    def restoreColors = "\u001B[0m"
    def beginColor = "\u001B["

    beginColor + weight + color + message + restoreColors
  }
}
