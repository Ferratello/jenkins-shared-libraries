package com.ferratello.logger

interface FancyLogger
{
  void debug(String message)

  void info(String message)

  void suggestion(String message)

  void warning(String message)

  void error(String message)

  void success(String message)
}