package com.ferratello.utils

class ParameterEncode implements Serializable
{
  String urlEncode(Map params)
  {
    return URLEncoder.encode(serialize(params), "UTF-8")
  }

  Map urlDecode(String encoded)
  {
    return deserialize(URLDecoder.decode(encoded, "UTF-8"))
  }

  private String serialize(Map params)
  {
    return params.inspect()
  }

  private Map deserialize(String serialized)
  {
    return Eval.me(serialized)
  }

}
