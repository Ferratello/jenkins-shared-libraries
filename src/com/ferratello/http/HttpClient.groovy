package com.ferratello.http

import groovy.json.JsonSlurperClassic

class HttpClient {
    def jenkins

    HttpClient(jenkins)
    {
        this.jenkins = jenkins
    }

    HttpResponse execute(request)
    {
        def response = jenkins.httpRequest(request)

        if (response.status in 200..201)
        {
            return HttpResponse.success(parse(response.content.toString()), response.status)
        }

        return HttpResponse.fail(parse(response.content.toString()), response.status)
    }

    @NonCPS
    private static parse(String content)
    {
        def slurper = new JsonSlurperClassic();
        return ["value": slurper.parseText(content)]
    }
}



