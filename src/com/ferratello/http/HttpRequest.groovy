package com.ferratello.http

class HttpRequest {
    private String url
    private String username
    private String password

    HttpRequest(String url, String username, String password) {

        this.password = password
        this.username = username
        this.url = url
    }

    Map toMap() {
        def requestMap = ["url" : url]
        requestMap.put('customHeaders', auth())
        return requestMap

    }

    private List<HashMap<String, String>> auth() {
        String auth = "$username:$password".bytes.encodeBase64().toString()
        return [["name": "Authorization", "value": "Basic $auth"]]
    }
}
