package com.ferratello.http

class HttpResponse {
    int statusCode
    boolean isSuccessful
    def content

    HttpResponse(content, boolean isSuccessful, statusCode) {
        this.content = content
        this.statusCode = statusCode
        this.isSuccessful = isSuccessful
    }

    static HttpResponse success(content, statusCode = 200) {
        return new HttpResponse(content, true, statusCode)
    }

    static HttpResponse fail(content, statusCode) {
        return new HttpResponse(content, false, statusCode)
    }
}
