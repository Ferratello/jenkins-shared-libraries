package com.ferratello.integrationTest

import com.ferratello.http.HttpClient
import com.ferratello.http.HttpRequest

class HttpClientIT {
    def jenkins

    HttpClientIT(jenkins) {
        this.jenkins = jenkins
    }

    public void testCallLocalJenkinsAPIT() throws Exception {
        def httpClient = new HttpClient(jenkins);
        def response = httpClient.execute(new HttpRequest("http://localhost:8080/api/json", "admin", "admin").toMap())

        assert response.isSuccessful
        assert response.content.value.nodeDescription == "the master Jenkins node"
    }
}
