package com.ferratello.integrationTest


import com.ferratello.logger.JenkinsEchoLogger

class FancyLoggerIT {
    def jenkins

    FancyLoggerIT(jenkins) {
        this.jenkins = jenkins
    }

    public void printASampleOfAllLogs() throws Exception {
        def logger = new JenkinsEchoLogger(jenkins);
        logger.debug("This is a DEBUG message")
        logger.info("This is an INFO message")
        logger.warning("This is an WARNING message")
        logger.error("This is an ERROR message")

        logger.success("Yeah SUCCESS!")
        logger.suggestion("This is warmly SUGGESTED, so don't be fool!")
    }
}
