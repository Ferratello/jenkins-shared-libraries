package com.ferratello.integrationTest

class IntegrationTest {
    def jenkinsSteps

    IntegrationTest(jenkinsSteps) {
        this.jenkinsSteps = jenkinsSteps
    }

    public void invokeAll(){
        new HttpClientIT(jenkinsSteps).testCallLocalJenkinsAPIT()
        new FancyLoggerIT(jenkinsSteps).printASampleOfAllLogs()
    }
}
