package com.ferratello.logger

public class JenkinsEchoLoggerTest extends GroovyTestCase {

    void testErrorContainsXEmoji() {
        FancyLogger logger = new JenkinsEchoLogger([echo: { assert it.contains("test_message") ;
                                                            assert it.contains("❌")
                                                            println it },
                                                    wrap: { myMap, cl -> cl() }])
        logger.error("test_message");
    }

    void testSuggestionContainsLightBulbEmoji() {
        FancyLogger logger = new JenkinsEchoLogger([echo: { assert it.contains("test_message") ;
                                                            assert it.contains("💡")
                                                            assert it.contains("\u001B[0m")
                                                            println it },
                                                    wrap: { myMap, cl -> cl() }])
        logger.suggestion("test_message");
    }
}
