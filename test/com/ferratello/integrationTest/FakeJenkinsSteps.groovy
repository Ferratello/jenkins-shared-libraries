package com.ferratello.integrationTest

class FakeJenkinsSteps {
    public Map httpRequest(request) {
        def response = new URL(request.url).openConnection().with { conn ->
            request.customHeaders.each {
                requests.add(it.get('name') ,it.get('value'))
            }
            def status = responseCode
            def content = conn.content.withReader { r -> r.text }
            return ["status": status, "content": content]
        }
        return response
    }

    public void echo(message){
        println message
    }

    void wrap(map, closure) {
        closure()
    }


}
