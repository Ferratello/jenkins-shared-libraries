package com.ferratello.http

class HttpRequestTest extends GroovyTestCase {

    void testToMapReturnPlainUrl() {
        def request = new HttpRequest("http://myurl.com", "::any user::", "::any password::")
        def requestMap = request.toMap()
        assert requestMap.url == "http://myurl.com"
    }

    void testAuthenticationReturnEncriptedObject() {
        def request = new HttpRequest("::any url::", "user", "password")
        def requestMap = request.toMap()
        assert requestMap.customHeaders == [["name": "Authorization", "value" : "Basic dXNlcjpwYXNzd29yZA=="]]
    }
}
