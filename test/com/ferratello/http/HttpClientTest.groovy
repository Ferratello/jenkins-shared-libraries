package com.ferratello.http

class HttpClientTest extends GroovyTestCase {

    public void testSuccessfulRequest() throws Exception {
        havingAResponseWith(status: 200, content: '{"name": "-ANY NAME-"}') {
            assert it.isSuccessful
            assert it.content.value.name == "-ANY NAME-"
        }
        havingAResponseWith(status: 201, content: '{"surname": "-ANY SURNAME-"}') {
            assert it.isSuccessful
            assert it.content.value.surname == "-ANY SURNAME-"
        }
    }

    public void testFailedRequest() throws Exception {
        havingAResponseWith(status: 404, content: '{"error": "This is an error in Json"}') {
            assert !it.isSuccessful
            assert it.content.value.error == 'This is an error in Json'
        }
    }

    private static void havingAResponseWith(response, assertion) {
        def jenkins = [httpRequest: { [status: response.status, content: response.content] }]

        assertion(new HttpClient(jenkins).execute([]))
    }
}


