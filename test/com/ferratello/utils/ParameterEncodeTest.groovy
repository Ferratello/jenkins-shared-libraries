package com.ferratello.utils

class ParameterEncodeTest extends GroovyTestCase {

    void testEncodedMapSingleString()
    {
        def input = ["token":"foo"]
        def serialized = new ParameterEncode().serialize(input)
        assert serialized.contains("foo")
        assert serialized.contains("token")
    }

    void testEncodedMapWithMultipleKeys()
    {
        def input = ["token":"foo", "env":"bar"]
        def result = new ParameterEncode().serialize(input)

        assert result.contains("foo")
        assert result.contains("bar")
    }

    void testUrlEncodeMap()
    {
        def input = ["token":"foo", "env":"b&r"]
        def result = new ParameterEncode().urlEncode(input)

        assert result.contains("foo")
        assert result.contains("b%26r")
    }

    void testUrlDecodeMap()
    {
        def encoded = new ParameterEncode().urlEncode(["token":"foo", "env":"b&r"])
        def result = new ParameterEncode().urlDecode(encoded)

        assert result.token == "foo"
        assert result.env == "b&r"
    }

    void testDeserialize()
    {
        def input = ["token":"foo", "env":"b&r"]
        def serialized = new ParameterEncode().serialize(input)
        def result = new ParameterEncode().deserialize(serialized)

        assert result.token == "foo"
        assert result.env == "b&r"
    }
}
